google_localizator
Hwangar

AIM
Use google translation api to add a "default translation" for non-translated
  strings.  Please, check Google-Translation API TOS.  What this module does
  is translate on each cron call a number of those strings, allowing later to
  check and to correct them

INSTALLATION & USE
As always,
  * activate the module
  * go to /admin/settings/language/gtranslate and fill your Google API key.  You
     NEED to have a google account to get one, later go to
     https://code.google.com/apis/console/?api=translate, activate tranlate api
     and copy the generated key there
  * now you can launch the cron and a number of strings will be google-translated
  * go to /admin/build/translate/gtranslate, here you can edit and correct them